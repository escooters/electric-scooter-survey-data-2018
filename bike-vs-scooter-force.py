# Quick script to compare extra force required to overcome an obstacle on an electric scooter vs a bicycle
# Contributors @ the Electric Scooter Guide -- https://electric-scooter.guide
import matplotlib
matplotlib.use('Tk agg')
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def obstacle_force(r,h):
    num = np.sqrt( (2*r*h) - (h*h) )
    den = r-h
    f = num/den
    return f

# Compute for one tire size
plt.figure()
scooterTireInches = 8/2.
bikeTireInches = 26/2.
obstacleHeightInches = np.linspace(0,3.5,100).astype(np.float)
forceBike = obstacle_force(bikeTireInches, obstacleHeightInches)
forceScooter = obstacle_force(scooterTireInches, obstacleHeightInches)
ratio = (forceScooter-forceBike)*100
sns.lineplot(obstacleHeightInches,ratio,palette='deep',lw=2)
plt.ylabel("% Extra Force Needed By Scooter")
plt.xlabel("Obstacle Height (inches)")
sns.despine()
plt.tight_layout()
plt.show()
